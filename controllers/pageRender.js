function register(req, res, next){
    res.render('register.pug');
}

function login(req, res, next){
    res.render('login.pug')
}

function showUser(req, res, next){
    res.render('showUser.pug')
}

function update(req, res, next){
    res.render('update.pug')
}

module.exports = {
    register,
    login,
    showUser,
    update
}