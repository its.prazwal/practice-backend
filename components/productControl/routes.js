const router = require('express').Router();
const controller = require('./productCtrl');
const fileUpload = require('./../../middlewares/fileHandler').upload;

router.route('/')
    .get(controller.showAll)
    .post(fileUpload('img', 'product'), controller.insertProduct)

router.route('/:id')
    .get(controller.showById)
    .put(fileUpload('img', 'product'),controller.updateProduct)
    .delete(controller.deleteById);

module.exports = router;