const productQuery = require('./productQuery');

function showAll(req, res, next) {
    productQuery.showProduct({})
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

function insertProduct(req, res, next) {
    if(req.loggedIn.role == 'buyer'){
        return next({
            msg: 'unauthorized access',
            status: 402
        })
    }
    if(req.fileError){
        return next({
            msg: 'invalid file format'
        })
    }
    if (req.file) {
        req.body.image = req.file.filename;
    }
    productQuery.insert(req.body)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next({
                msg: 'product insertion faliure'
            });
        });
}

function updateProduct(req, res, next) {
    productQuery.update(req.params.id, req.body)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function showById(req, res, next) {
    var data = { _id: req.params.id }
    productQuery.showProduct(data)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function deleteById(req, res, next) {
    var data = { _id: req.params.id }
    productQuery.deleteById(data)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next({
                msg: 'product no found'
            });
        })
}

module.exports = {
    showAll,
    insertProduct,
    updateProduct,
    showById,
    deleteById
}