const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mainCategoryData = ['Mens','Womens','Kids'];
const categoryData = ['Clothing','Traditional Clothing','Shoes','Bags'];

var productSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    brand: {
        type: String,
    },
    mainCategory: {
        type: String,
        required: true,
        enum: mainCategoryData
    },
    category: {
        type: String,
        required: true,
        enum: categoryData
    },
    subCatergory: {
        type: String
    },
    color: {
        type: [String]
    },
    price: {
        type: Number,
        required: true
    },
    size: {
        type: [String],
        required: true
    },
    description: String,
    image: String,
    discount: {
        discountedItem: Boolean,
        discountType: String,
        discountValue: Number
    },
    status: {
        type: String,
        enum: ['available', 'sold', 'booked'],
        default: 'available'
    },
    quantity: Number,
    warranty: {
        warrantyState: Boolean,
        warrantyValue: String,
    },
    seller: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
});

var product = mongoose.model('product', productSchema);

module.exports = product;