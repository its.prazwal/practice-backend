module.exports = function (data1, data2) {
    if (data2.name)
        data1.name = data2.name;
    if (data2.brand)
        data1.brand = data2.brand;
    if (data2.mainCategory)
        data1.mainCategory = data2.mainCategory;
    if (data2.category)
        data1.category = data2.category;
    if (data2.color)
        data1.color = data2.color.split(',');
    if (data2.subCategory)
        data1.subCategory = data2.subCategory;
    if (data2.price)
        data1.price = data2.price;
    if (data2.batchNo)
        data1.batchNo = data2.batchNo;
    if (data2.description)
        data1.description = data2.description;
    if (data2.image)
        data1.image = data2.image;
    if (data2.discountedItem) {
        data1.discount = {
            discountedItem: data2.discountedItem,
            discountType: data2.discountedItem == 'false' ? null : data2.discountType,
            discountValue: data2.discountedItem == 'false' ? null : data2.discountValue
        };
    }
    if(data2.warrantyState){
        data1.warranty ={
            warrantyState: data2.warrantyState,
            warrantyValue: data2.warrantyState == 'false' ? null: data2.warrantyValue
        }
    };
    if (data2.status)
        data1.status = data2.status;
    if (data2.seller)
        data1.seller = data2.seller;

    return data1;
}