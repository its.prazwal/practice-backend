const productModel = require('./models/productModel');
const mapProduct = require('./models/mapProduct');
const fileRemover = require('./../../middlewares/fileHandler').fileRemover;

function showProduct(data) {
    return productModel.find(data);
}

function insert(data) {
    var newProduct = new productModel(data);
    var newMappedProduct = mapProduct(newProduct, data);
    return newMappedProduct.save();
}

function update(id, data) {
    return new Promise(function (resolve, reject) {
        productModel.findById(id)
            .exec(function (err, productDetail) {
                if (err) {
                    return reject(err);
                }
                if (!productDetail) {
                    return reject({
                        msg: 'product not found'
                    });
                }
                var updatedProduct = mapProduct(productDetail, data);
                updatedProduct.save(function (err, updated) {
                    if (err) {
                        reject({
                            msg: 'product not updated'
                        })
                    }
                    else {
                        resolve(updated);
                    }
                })
            })
    })
}

function deleteById(id) {
    productModel.findById(id)
        .then(data => fileRemover(data.image));
    return productModel.findByIdAndDelete(id);
}

module.exports = {
    showProduct,
    insert,
    update,
    deleteById
}