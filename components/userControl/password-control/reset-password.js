const userModel = require('./../models/userModel');
const passwordHash = require('password-hash');

function resetPassword(req, res, next) {
    userModel.findOne(req.params)
        .exec(function (err, user) {
            if (err) { return next(err) };
            if (!user) { return next({ msg: 'Password reset token invalid' }); }
            if (Date.now() > new Date(user.resetExpiry).getTime()) {
                return next({ msg: 'Password reset time expired.' });
            }
            user.resetExpiry = null;
            user.resetToken = null;
            user.passWord = passwordHash.generate(req.body.passWord);
            user.save(function (err, done) {
                if (err) { return next(err) }
                res.status(200).json(done);
            })

        })
}

module.exports = resetPassword;