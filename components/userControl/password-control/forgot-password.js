const userQuery = require('../query/userQuery');
const randomSt = require('randomstring');

const nodemailer = require('nodemailer');
const sender = nodemailer.createTransport({
    service: 'Gmail',
    secure: true,
    auth: {
        user: 'site.testbypz@gmail.com',
        pass: 'mepraz20'
    }
})

function prepareMail(data) {
    let mailData = {
        from: 'PZ Product Store <noreply@pzproductstore.com>',
        to: data.emailId,
        subject: 'Forgot Password',
        html:
            `<p>Hi, <strong>${data.firstName} ${data.lastName}</strong></p>
        <p>We have received a request that you want to reset your password.</p>
        </br>
        <p><a href='${data.link}'>Reset Password >></a></p>
        <p>This link will expire in 24 hours and can be used only once.</p>
        </br>
        <p>If you don't want to change your password or didn't request this, please ignore and delete this message.</p>
        <p>Thank You</p>
        <p>The PZ Product Store Team</p>`
    }
    return (mailData);
}

function forgotPassword(req, res, next) {
    userQuery.findOneUser(req.body)
        .then(function (user) {
            if (user) {
                const resetToken = randomSt.generate(20);
                const resetExpiry = new Date(Date.now() + 1000 * 60 * 60 * 24);
                var mailData = {
                    firstName: user.firstName,
                    lastName: user.lastName,
                    emailId: user.emailId,
                    link: `${req.headers.origin}/reset-password/${resetToken}`
                }
                const mailContent = prepareMail(mailData);
                userQuery.update(user._id, { resetExpiry, resetToken })
                    .then(function (data) {
                        sender.sendMail(mailContent, function (err, done) {
                            if (err) { return next(err) }
                            res.json(done);
                        })
                    })
                    .catch(function (err) { next(err) });
            } else {
                next({
                    msg: 'Email Address not registered.'
                })
            }
        })
        .catch(function(err) { next(err) })
}

module.exports = forgotPassword;