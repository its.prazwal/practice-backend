const userModel = require('./../models/userModel');
const passwordHash = require('password-hash');

function changePassword(req, res, next) {
    const { previousPassword, newPassword } = req.body
    userModel.findOne({ _id: req.userId })
        .exec(function (err, user) {
            if (err) { return next(err) };
            var isMatched = passwordHash.verify(previousPassword, user.passWord);
            if (!isMatched) { return next({ msg: 'Previous password not matched' }) }
            user.passWord = passwordHash.generate(newPassword);
            user.save(function (err, saved) {
                if (err) { return next(err) };
                res.status(200).json({ msg: 'Password changed Sucessfully' })
            })
        })
}

module.exports = changePassword;