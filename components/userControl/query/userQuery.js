const userModel = require('./../models/userModel');
const mapUser = require('./../models/map_user');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('../../../config');
const fileRemover = require('./../../../middlewares/fileHandler').fileRemover;

//used by frontend

function insert(datas) {
    var newUser = new userModel(datas);
    var newMappedUser = mapUser(newUser, datas);
    newMappedUser.passWord = passwordHash.generate(datas.passWord);
    return newMappedUser.save();
}

function findExistingUser(data) {
    return userModel.find(data);
}

function findOneUser(data) {
    return userModel.findOne(data);
}

function userLogin(un, pw) {
    return new Promise(function (resolve, reject) {
        findOneUser({ $or: [{ userName: un }, { emailId: un }] })
            .then(function (user) {
                var isMatched = passwordHash.verify(pw, user.passWord)
                if (isMatched) {
                    var token = jwt.sign({ id: user._id, role: user.role }, config.jwtSecret);
                    update(user._id, { status: 'online' })
                        .then(null);
                    return resolve({ user: user.userName, id: user._id, role: user.role, token })
                }
                else { reject({ msg: 'Username or Password is Incorrect!!!', status: 401 }) }
            })
            .catch(function (err) { reject({ msg: 'Username or Password is Incorrect!!!', status: 401 }) })
    })
}

function remove(id) {
    userModel.findById({ _id: id })
        .then(function (data) { data.image ? fileRemover(data.image) : null });
    return userModel.findByIdAndRemove(id);
}

function update(id, data) {
    return new Promise(function (resolve, reject) {
        userModel.findById(id)
            .exec(function (err, user) {
                if (err) {
                    return reject(err);
                }
                if (!user) {
                    return reject({
                        msg: 'User Not Found',
                        status: 400
                    })
                }
                var updatedUser = mapUser(user, data);
                updatedUser.save(function (err, updated) {
                    if (err) {
                        reject({
                            msg: 'User Update Failure',
                            status: 400
                        });
                    }
                    else {
                        resolve({
                            msg: 'User Updated',
                            status: 200
                        });
                    }
                });
            });
    });
}

module.exports = {
    findOneUser,
    insert,
    findExistingUser,
    userLogin,
    update,
    remove
}
