const router = require("express").Router();
const controller = require("./controllers/userCtrl");
const authenticate = require("./../../middlewares/authenticate");
// const authorize = require('./../../middlewares/roleSector');
const fileUpload = require("./../../middlewares/fileHandler").upload;
const forgotPassword = require("./password-control/forgot-password");
const resetPassword = require("./password-control/reset-password");
const changePassword = require("./password-control/change-password");

router
  .route("/register")
  .get(controller.checkExistingUser)
  .post(controller.insert);

router.route("/login").post(controller.login);

router
  .route("/task/:id")
  .get(authenticate, controller.getById)
  .put(authenticate, controller.update)
  .delete(authenticate, controller.remove);

router.route("/forgot-password").post(forgotPassword);

router.route("/reset-password/:resetToken").post(resetPassword);

router.route("/role/:role").get(authenticate, controller.getByRole);

router.route("/change-password").post(authenticate, changePassword);

router
  .route("/upload/:id")
  .post(fileUpload("img", "user"), controller.uploadPic);

//task remaining
router.route("/details").get(controller.userDetails);

router
  .route("/update")
  .get(controller.checkExistingUser)
  .put(authenticate, controller.update);

module.exports = router;
