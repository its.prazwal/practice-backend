module.exports = function (data1, data2) {
    if (data2.firstName)
        data1.firstName = data2.firstName;
    if (data2.lastName)
        data1.lastName = data2.lastName;
    if (data2.userName)
        data1.userName = data2.userName;
    if (data2.dateOfBirth)
        data1.dateOfBirth = data2.dateOfBirth.split('T')[0];
    if (data2.gender)
        data1.gender = data2.gender;
    if (data2.country)
        data1.country = data2.country;
    if (data2.emailId)
        data1.emailId = data2.emailId;
    if (data2.mobileNo)
        data1.mobileNo = data2.mobileNo;
    if (data2.status)
        data1.status = data2.status;
    if (data2.passWord)
        data1.passWord = data2.passWord;
    if (data2.role)
        data1.role = data2.role;
    if (data2.image)
        data1.image = data2.image;
    if (data2.resetToken)
        data1.resetToken = data2.resetToken;
    if (data2.resetExpiry)
        data1.resetExpiry = data2.resetExpiry;
    return data1;
};