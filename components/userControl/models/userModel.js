const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    userName: {
        type: String,
        required: true,
        unique: true
    },
    role: {
        type: String,
        enum: ['admin','seller','user'],
        default: 'user'
    },
    dateOfBirth:{
        type: String
    },
    mobileNo:{
        type: Number,
        required: true,
        unique: true
    },
    emailId:{
        type: String,
        unique: true,
        required: true
    },
    gender: {
        type: String
    },
    status: {
        type: String,
        enum: ['online', 'offline', 'away'],
        default: 'offline'
    },
    passWord: {
        type: String,
        required: true
    },
    resetToken: {
        type: String,
    },
    resetExpiry: {
        type: Date
    },
    image: {
        type: String
    }
});
var user = mongoose.model('user', userSchema);
module.exports = user;