const userQuery = require("../query/userQuery");
const typeCtrl = require("./typeCtrl");
const fileRemover = require("./../../../middlewares/fileHandler").fileRemover;

//used by front end

//Should be changed using EVENTS
function checkExistingUser(req, res, next) {
  userQuery
    .findExistingUser(req.query)
    .then(function (data) {
      data.length
        ? res.status(400).json(data[0]._id)
        : res.status(200).json("success");
    })
    .catch(function (err) {
      res.status(200).json("success");
    });
}

function uploadPic(req, res, next) {
  if (req.fileError) {
    return next({ msg: "invalid file format" });
  }
  userQuery.findOneUser({ _id: req.params.id }).then(function (user) {
    if (user.image) {
      fileRemover(`userimg/${user.image}`);
    }
  });
  if (req.file) {
    var image = req.file.filename;
  }
  userQuery
    .update(req.params.id, { image })
    .then(function (data) {
      res.status(data.status).json(data.msg);
    })
    .catch(function (err) {
      next(err);
    });
}

function insert(req, res, next) {
  userQuery
    .insert(req.body)
    .then(function (data) {
      res.status(200).json("user inserted");
    })
    .catch(function (err) {
      next(err);
    });
}

function login(req, res, next) {
  userQuery
    .userLogin(req.body.userName, req.body.passWord)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err);
    });
}

function getById(req, res, next) {
  userQuery
    .findOneUser({ _id: req.params.id })
    .then(function (user) {
      res.status(200).json(user);
    })
    .catch(function (err) {
      next(err);
    });
}

function remove(req, res, next) {
  userQuery
    .remove({ _id: req.params.id })
    .then(function (data) {
      res.status(200).json("user deleted");
    })
    .catch(function (err) {
      next(err);
    });
}

function update(req, res, next) {
  userQuery
    .update({ _id: req.params.id }, req.body)
    .then(function (data) {
      res.status(data.status).json(data.msg);
    })
    .catch(function (err) {
      next(err);
    });
}

function getByRole(req, res, next) {
  userQuery
    .findExistingUser(req.params)
    .then(function (users) {
      res.status(200).json(users);
    })
    .catch(function (err) {
      next(err);
    });
}

//not used by frontend
function userDetails(req, res, next) {
  userQuery
    .findUser(req.query)
    .then((user) => {
      res.status(200).json(user);
    })
    .catch((err) => next(err));
}

function getByType(req, res, next) {
  if (req.params.userName) {
    if (req.loggedIn.userName != req.params.userName) {
      return next({
        msg: "invalid username request",
      });
    }
    var condition = { userName: req.params.userName };
  } else {
    var condition = typeCtrl(req.body);
    if (condition == null) {
      return next({ msg: "request invalid or key: value pair mismatched" });
    }
  }
  userQuery
    .findAny(condition)
    .then(function (data) {
      if (data.length < 1) {
        next({
          msg: "user not found",
        });
      } else {
        res.status(200).json(data);
      }
    })
    .catch(function (err) {
      next(err);
    });
}

module.exports = {
  checkExistingUser,
  insert,
  uploadPic,
  remove,
  update,
  login,
  getByRole,
  userDetails,
  getById,
  getByType,
};
