module.exports = function (type) {
    let showType = Object.keys(type)[0];
    let showvalue = Object.values(type)[0];
    switch (showType) {
        case 'role':
            return condition = { role: showvalue };
        case 'id':
            if(showvalue.length != 24){
                return condition = null;
            }
            return condition = { _id: showvalue };
        case 'all':
            return condition = {};
        default:
            return condition = null;
    }
}