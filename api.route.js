const router = require('express').Router();
const authenticate = require('./middlewares/authenticate');
const userRoute = require('./components/userControl/routes');
const productRoute = require('./components/productControl/routes');

router.use('/user', userRoute);
router.use('/product', authenticate, productRoute);

module.exports = router;