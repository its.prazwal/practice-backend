const mongoose = require('mongoose');
const cnxurl = 'mongodb://localhost:27017';
const dbname = 'PZProductStore';

//connecting mongoose database
mongoose.connect(cnxurl + '/' + dbname, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }, function (err, done) {
    if (err) {
        console.log('Database connection unsucesfull');
    } else {
        console.log('Database *' + dbname + '* has connected at port: 27017');
    }
});