module.exports = function(req, res, next){
    var role = req.loggedIn.role;
    if(role != 'admin'){
        return next({
            msg: 'permission not granted'
        })
    }
    next();
}