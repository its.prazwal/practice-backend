const multer = require('multer');
const fs = require('fs');
const path = require('path');

var userStorage = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, new Date().getTime() + '-' + file.originalname);
    },
    destination: function (req, file, cb) {
        cb(null, './uploads/image/userimg');
    }
})

var productStorage = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, new Date().getTime() + '-' + file.originalname);
    },
    destination: function (req, file, cb) {
        cb(null, './uploads/image/productsimg');
    }
})

function fileFilter(req, file, cb) {
    var mimeType = file.mimetype.split('/')[0];
    if (mimeType == 'image') {
        cb(null, true);
    } else {
        req.fileError = true;
        cb(null, false);
    }
}

function upload(type, imageOf){
    if(imageOf == 'user'){
        var mul = multer({
            storage: userStorage,
            fileFilter
        });
    }else if(imageOf == 'product'){
        var mul = multer({
            storage: productStorage,
            fileFilter
        });
    }
    return mul.single(type);
}

function fileRemover(data) {
    fs.unlink(path.join(process.cwd(), 'uploads/image/' + data), function (err, done) {
        if (err) {
            console.log('file removing error');
        }
        else {
            console.log('file removed');
        }
    });
}

module.exports = { upload, fileRemover};