var jwt = require('jsonwebtoken');
var config = require('./../config/index');
var userModel = require('./../components/userControl/models/userModel')

module.exports = function (req, res, next) {
    var token;
    if (req.headers['x-access-token']) { token = req.headers['x-access-token']; }
    else if (req.headers['authorization']) { token = req.headers['authorization']; }
    else if (req.headers['token']) { token = req.headers['token']; }
    else { next({ msg: 'Token not found' }) }
    if (token) {
        jwt.verify(token, config.jwtSecret, function (err, logIn) {
            if (err) {
                return next({
                    msg: 'unauthorize access',
                    status: 402
                });
            }
            userModel.findById(logIn.id)
                .exec(function (err, user) {
                    if (err) { console.log('usernot found'); return next(err); }
                    if (!user) {
                        return next({
                            msg: 'user removed from system'
                        });
                    }
                    req.userRole = user.role;
                    req.userId = user._id;
                    next();
                })
        })
    }
    else { next({ msg: 'Unauthorized Token' }) }
}