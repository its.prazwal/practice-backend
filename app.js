const express = require('express');
const app = express();
const cors = require('cors');
const path = require('path');

//importing API routers
const apiRoute = require('./api.route');

//settings
app.set('port', 9990);
// app.set('view engine', pug);
// app.set('views', path.join(__dirname,'pugFiles'));

//running database
require('./config/database.js');

//loading files
app.use(express.static('files'));
app.use('/file', express.static(path.join(__dirname, 'uploads')));

//request body parser
app.use(express.urlencoded({
    extended: true
}))

//json parser
app.use(express.json());

//routing urls
app.use(cors());
app.use('/api', apiRoute);

//error handling middleware
app.use(function (err, req, res, next) {
    res.status(err.status || 404).json({
        msg: err.msg || 'Page Not Found !!!'
    })
})

//server connecting
app.listen(app.get('port'), function (err, connected) {
    if (err) {
        console.log('server connection unsucessfull');
    }
    else {
        console.log('server connection succesfull');
        console.log('listening at port: ', app.get('port'));
        console.log('Press CTRL+C to exit');
    }
})